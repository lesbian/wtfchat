# Contributors

This project contains contributions from:

* [Mr Stallion, esq.](https://github.com/mrstallion) (original F-Chat Rising Fork)
* [Twilight Sparkle](https://gitlab.com/lesbian)
* [ButterCheezii](https://github.com/ButterCheezii)
* [FatCatClient](https://github.com/FatCatClient)
* [F-List Team](https://github.com/f-list) (original F-Chat 3.0 client)
